$.fn.memenu = function (e) {
    function r() {
        u();
        i()
    }

    function i() {
        $(".memenu li").bind("mouseover", function () {
            $(this).children(".dropdown, .mepanel").stop().fadeIn(t.interval)
        }).bind("mouseleave", function () {
            $(this).children(".dropdown, .mepanel").stop().fadeOut(t.interval)
        })
    }

    function u() {
        $(".memenu > li").show(0);
        $(".memenu > li.showhide").hide(0)
    }

    var t = {interval: 250};
    var n = 0;
    $(".memenu").prepend("<li class='showhide'><span class='title'>Каталог</span></li>");
    r();
    $(window).resize(function () {
        r()
    })
};


var h_hght = 232; // высота шапки

$(window).scroll(function() {
    var top = $(this).scrollTop();
    if (top > h_hght) {
        $(".search-area").addClass("nav-fixed");
    }
    if (top < h_hght) {
        $(".search-area").removeClass("nav-fixed");
    }
});
