function addToCart(id, name, price, imgPath) {
    var product = {
        "product_id": id,
        "name": name,
        "price": price,
        "img_path": imgPath
    };
    simpleCart.add(product);
    $(".cart").effect("shake", {times:2}, 400);
}
$(document).ready(function() {
    $(".memenu").memenu();

});