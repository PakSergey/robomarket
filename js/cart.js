function loadCart() {
    var cartItems = simpleCart.items();
    var cart = $(".cart-items");
    cartItems.forEach(
        function (item, i, arr) {
            var cartItem = $(document.createElement("div")).addClass("row cart-item  item-row").attr("id", item.id());
            var cartItemImg = $(document.createElement("div")).addClass("col-3 cart-item-col").append(
                $(document.createElement("img")).attr("src", item.get("img_path"))
            );
            var cartItemName = $(document.createElement("div")).addClass("col-3 cart-item-col").append(
                $(document.createElement("div")).append(item.get("name"))
            );
            var cartItemQuantity = $(document.createElement("div")).addClass("col-3 cart-item-col").append(
                $(document.createElement("div")).append(
                    $(document.createElement("input")).attr("type", "number").attr("value", item.quantity()).addClass("cart-item-number")
                )
            );
            var cartItemPrice = $(document.createElement("div")).addClass("col-2 cart-item-col").append(
                $(document.createElement("div")).append(item.get("price") + "руб.")
            );
            var cartItemRemove = $(document.createElement("div")).addClass("col-1 cart-item-col").append(
                $(document.createElement("div")).append(
                    $(document.createElement("a")).addClass("remove-item").append(
                        $(document.createElement("span")).append(
                            $(document.createElement("i")).addClass("fa fa-times")
                        )
                    )
                )
            );
            cartItem.append(cartItemImg,cartItemName,cartItemQuantity,cartItemPrice,cartItemRemove);
            cart.append(cartItem);
        }
    );
    var cartTotalLine = $(document.createElement("div")).addClass("row total-line").append(
        $(document.createElement("div")).addClass("col-7")
    ).append(
        $(document.createElement("div")).addClass("col-3").append(
            "Итого <span id='cart-total'>" + simpleCart.total() + "</span> руб."
        )
    ).append(
        $(document.createElement("div")).addClass("col-2").append(
            $(document.createElement("button")).addClass("btn btn-color").append(
                "Оформить заказ"
            )
        )
    );
    cart.append(cartTotalLine);
    $(".cart-item-number").change(function () {
        var itemRow = $(this).closest(".item-row");
        var itemId = itemRow.attr("id");
        var quantity = $(this).val();
        if (quantity <= 0) {
            quantity = 0;
        }
        simpleCart.find(itemId).set("quantity", quantity);
        simpleCart.save();
        updateTotal();
        if (quantity <= 0) {
            removeItem(itemRow);
        }
    });

    $(".remove-item").click(function () {
        var itemRow = $(this).closest(".item-row");
        simpleCart.find(itemRow.attr("id")).remove();
        simpleCart.save();
        updateTotal();
        removeItem(itemRow);
    });

    function updateTotal() {
        $("#cart-total").empty().append(simpleCart.total());
    }

    function removeItem(itemRow) {
        $(itemRow).fadeOut(400);
    }
}
$(document).ready(function() {
    $(".memenu").memenu();
    loadCart();
});
