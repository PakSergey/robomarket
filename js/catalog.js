$(document).ready(function() {
    $(".memenu").memenu();
    $( "#slider-range" ).slider({
        range: true,
        min: 0,
        max: 500,
        values: [ 100, 300 ],
        slide: function( event, ui ) {
            $( "#price-from" ).val(ui.values[ 0 ]);
            $( "#price-to" ).val(ui.values[ 1 ]);
        }
    });

    $( "#price-from" ).val(100);
    $( "#price-to" ).val(300);

    $(".price-input").keyup(
        function () {
            $("#slider-range").slider({
                values: [ $("#price-from").val(), $("#price-to").val() ]
            });
        }
    );
});
function addToCart(id, name, price, imgPath) {
    var product = {
        "product_id": id,
        "name": name,
        "price": price,
        "img_path": imgPath
    };
    simpleCart.add(product);
    $(".cart").effect("shake", {times:2}, 400);
}
